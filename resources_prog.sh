#!/bin/bash

##Colors
r='\e[1;91m' && g='\e[1;92m' && y='\e[1;93m' && b='\e[1;96m' && w='\e[1;97m' && n='\e[0m'

# Check if $1 is empty
if [ -z "$1" ]; then
  echo -e "\n$r[error] : Please give a process name !" && exit 1
fi

# Get Process PID
pid_APP=$(pgrep -f "$1" | head -n 1)

# Compare PIDs
if [ "$$" == "$(($pid_APP + 1))" ] || [ "$$" == "$pid_APP" ]; then

  # [ "$$" == "$pid_APP" ] : test si le script est lance directement dans le terminal
  # [ "$$" == "$(($pid_APP + 1))" ] : test si le script est lance directement via serveur Flask

  echo -e "\n$r[Error : No Process Named [ $1 ] !\n"
  exit 1
fi

# This Function give the process memory usage
get_memory() {
  # Memory Usage (Ko)
  #memory_usage_Ko=$(pmap -x $pid_APP | grep "total" | head -n 1 | awk '{print $3}')
  #echo "$memory_usage_Ko"

  # Memory Usage (Mo)
  #memory_usage_Mo=$(pmap -x $pid_APP | grep "total" | head -n 1 | awk '{print $3/1024}')
  #echo "$memory_usage_Mo"

  # Memory Usage (Go)
  memory_usage_Go=$(pmap -x $pid_APP | grep "total" | head -n 1 | awk '{print ($3/1024)/1024}')
  echo "$memory_usage_Go"
}

memory=$(get_memory "$1")
cpu_percentage=$(ps -p $pid_APP -o %cpu | awk 'NR==2')

echo -e "$memory $cpu_percentage"

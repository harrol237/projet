from flask import Flask, request, jsonify
import subprocess
import os

app = Flask(__name__)


# Route pour la récupération des ressources
# Exemple "http://localhost:5000/api/v1/ressources?process_name=chrome"
@app.route("/api/v1/ressources", methods=["GET"])
def get_resources():

    process_name = request.args.get("process_name")
    print("Process Name : ", process_name)
    # Vérifier si le nom du processus est fourni
    if not process_name:
        return jsonify({"error": "no process name given"}), 400

    try:

        # get resource use by process name
        result = subprocess.check_output(
            ["./resources_prog.sh {}".format(process_name)],
            shell=True,
            universal_newlines=True,
        )

        process_memory, cpu_percent = map(str, result.strip().split())
        # print(process_memory, "-", cpu_percent)
        resources_usage = {
            "cpu_percent_usage": cpu_percent.strip(),
            "Go_memory_usage": process_memory.strip(),
        }

        # return resources datas
        return jsonify(resources_usage), 200

    except subprocess.CalledProcessError as e:
        if e.returncode == 1:
            return jsonify({"error": "Process not found"}), 404


if __name__ == "__main__":
    app.run(debug=True)
